import { NgModule } from '@angular/core';
import { AssociationService } from './association.service';
import { AssociationRoutingModule } from './association-routing.module';
import { AssociationComponent } from './association.component';

@NgModule({
    imports: [
        AssociationRoutingModule
    ],
    exports: [
        AssociationComponent
    ],
    declarations: [
        AssociationComponent
    ],
    providers: [
        AssociationService
    ]
})
export class AssociationModule { }
