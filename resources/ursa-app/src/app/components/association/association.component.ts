import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-association',
    templateUrl: './association.component.html'
})

export class AssociationComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {

    }
}
