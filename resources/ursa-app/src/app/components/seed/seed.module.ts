import { NgModule } from '@angular/core';
import { SeedRoutingModule } from './seed-routing.module';
import { SeedComponent } from './seed.component';
import { SeedService } from './seed.service';

@NgModule({
    imports: [
        SeedRoutingModule
    ],
    exports: [
        SeedComponent
    ],
    declarations: [
        SeedComponent
    ],
    providers: [
        SeedService
    ]
})
export class SeedModule { }
