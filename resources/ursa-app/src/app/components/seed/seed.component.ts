import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-seed',
    templateUrl: './seed.component.html'
})

export class SeedComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {

    }
}
