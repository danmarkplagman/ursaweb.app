import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-credential',
    templateUrl: './credential.component.html'
})

export class CredentialComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {

    }
}
