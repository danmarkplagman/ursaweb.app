import { NgModule } from '@angular/core';
import { CredentialRoutingModule } from './credential-routing.module';
import { CredentialComponent } from './credential.component';
import { CredentialService } from './credential.service';

@NgModule({
    imports: [
        CredentialRoutingModule
    ],
    exports: [
        CredentialComponent
    ],
    declarations: [
        CredentialComponent
    ],
    providers: [
        CredentialService
    ]
})
export class CredentialModule { }
