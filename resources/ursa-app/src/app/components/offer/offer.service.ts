import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/do';

@Injectable()
export class OfferService {

    private _prefix = 'offer/';

    constructor(
        private _http: HttpClient
    ) {
    }

    public getOffers(company_id: number, feature_id: number): Observable<Response> {

        // tslint:disable-next-line:max-line-length
        return this._http.get(environment.API_ENDPOINT + this._prefix + 'lists/' + company_id + '/' + feature_id).do((response: any) => response);
    }

    public getOfferDetails(offer_id: number): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'details/' + offer_id).do((response: any) => response);
    }

    public getTypes(): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'types').do((response: any) => response);
    }

    public getCategories(): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'categories').do((response: any) => response);
    }

    public getFeatures(): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'features').do((response: any) => response);
    }

    // tslint:disable-next-line:max-line-length
    public createOffer(title: string, cta: string, button_label: string, status: number, offer_type_id: number, offer_category_id: number, company_id: number, features: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('title', title);
        data.append('cta', cta);
        data.append('button_label', button_label);
        data.append('status', status.toString());
        data.append('offer_type_id', offer_type_id.toString());
        data.append('offer_category_id', offer_category_id.toString());
        data.append('company_id', company_id.toString());
        data.append('features', features);

        return this._http.post(environment.API_ENDPOINT + this._prefix + 'create', data.toString()).do((response: any) => response);
    }

    // tslint:disable-next-line:max-line-length
    public updateOffer(id: number, title: string, cta: string, button_label: string, status: number, offer_type_id: number, offer_category_id: number, company_id: number, features: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('id', id.toString());
        data.append('title', title);
        data.append('cta', cta);
        data.append('button_label', button_label);
        data.append('status', status.toString());
        data.append('offer_type_id', offer_type_id.toString());
        data.append('offer_category_id', offer_category_id.toString());
        data.append('company_id', company_id.toString());
        data.append('features', features);

        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'update', data.toString()).do((response: any) => response);
    }

    public removeOffer(id: number): Observable<Response> {

        return this._http.delete(environment.API_ENDPOINT + this._prefix + 'delete/' + id).do((response: any) => response);
    }
}
