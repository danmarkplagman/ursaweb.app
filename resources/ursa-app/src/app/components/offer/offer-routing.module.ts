import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfferComponent } from './offer.component';

const routes: Routes = [
  { path: '', component: OfferComponent, children: [
    { path: 'category', loadChildren: './category/category.module#OfferCategoryModule' },
] }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class OfferRoutingModule {}
