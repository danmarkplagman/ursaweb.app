import { NgModule } from '@angular/core';

import { OfferComponent } from './offer.component';
import { OfferRoutingModule } from './offer-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OfferService } from './offer.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OfferRoutingModule
    ],
    exports: [
        OfferComponent
    ],
    declarations: [
        OfferComponent
    ],
    providers: [
        OfferService
    ]
})
export class OfferModule { }
