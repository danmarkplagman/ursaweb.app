import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OfferCategoryRoutingModule } from './category-routing.module';
import { OfferCategoryComponent } from './category.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OfferCategoryRoutingModule
    ],
    exports: [
        OfferCategoryComponent
    ],
    declarations: [
        OfferCategoryComponent
    ],
    providers: [

    ]
})
export class OfferCategoryModule { }
