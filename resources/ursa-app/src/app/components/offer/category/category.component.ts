import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { OfferService } from '../offer.service';
import { ActivatedRoute } from '@angular/router';

declare var jQuery: any;

@Component({
    selector: 'app-offer-category',
    templateUrl: './category.component.html'
})

export class OfferCategoryComponent implements OnInit, AfterViewInit, OnDestroy {

    public offers: Array<any> = [];
    public types: Array<any> = [];
    public errors: Array<any> = [];
    public is_content_loading: boolean;
    public offer_features: Array<any> = [];
    public offer_id: number;

    /* Model Properties */
    public title: string;
    public cta: string;
    public button_label: string;
    public status: number;
    public offer_type_id: number;
    public offer_category_id: number;
    public e_title: string;
    public e_cta: string;
    public e_button_label: string;
    public e_status: number;
    public e_offer_type_id: number;
    public e_offer_category_id: number;

    private _offerSubscription: Subscription;
    private _typeSubscription: Subscription;

    constructor(
        private _route: ActivatedRoute,
        private _offerService: OfferService
    ) {
        this.title = '';
        this.cta = '';
        this.button_label = '';
        this.status = null;
        this.offer_type_id = null;
        this.offer_category_id = null;
        this.offer_features = [];
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        this._route.params.subscribe((params: any) => {

            this.is_content_loading = true;

            this._offerSubscription = this._offerService.getOffers(1, params.category_id).subscribe((response: any) => {

                this.is_content_loading = false;

                if (!response) {
                    this.offers = [];
                    return;
                }

                this.offer_category_id = params.category_id;
                this.offers = response.offers;

            }, (error: Error) => console.log(error.message));

            this._typeSubscription = this._offerService.getTypes().subscribe((response: any) => {

                if (!response) {
                    return;
                }

                this.types = response.types;

            }, (error: Error) => console.log(error.message));

        });
    }

    ngOnDestroy() {

        if (this._offerSubscription) {
            this._offerSubscription.unsubscribe();
        }
        if (this._typeSubscription) {
            this._typeSubscription.unsubscribe();
        }
    }

    public getDetails(index: number): void {

        const offer = this.offers[index];

        this.offer_id = offer.id;
        this.e_title = offer.title;
        this.e_cta = offer.cta;
        this.e_button_label = offer.button_label;
        this.e_status = offer.status;
        this.e_offer_type_id = offer.offer_type_id;
        this.e_offer_category_id = offer.offer_category_id;
        this.offer_features = offer.features;

        jQuery('#editoffermodal').modal('show');
    }

    public addFeature(): void {

        let feature = (<HTMLInputElement>document.getElementById('offer_feature')).value;
        let benefit = (<HTMLInputElement>document.getElementById('offer_benefit')).value;

        if (!feature || !benefit) {
            return;
        }

        this.offer_features.push({
            feature: feature,
            benefits: benefit
        });

        feature = null;
        benefit = null;
    }

    public addUpdateFeature(): void {

        let feature = (<HTMLInputElement>document.getElementById('e_offer_feature')).value;
        let benefit = (<HTMLInputElement>document.getElementById('e_offer_benefit')).value;

        if (!feature || !benefit) {
            return;
        }

        this.offer_features.push({
            feature: feature,
            benefits: benefit
        });

        feature = null;
        benefit = null;
    }

    public removeFeature(index: number): void {

        this.offer_features.splice(index, 1);
    }

    public create(): void {

        this.errors = [];

        if (!this.title) {
            this.errors['title'] = true;
            return;
        }
        if (!this.cta) {
            this.errors['cta'] = true;
            return;
        }
        if (!this.button_label) {
            this.errors['button_label'] = true;
            return;
        }
        if (!this.status) {
            this.errors['status'] = true;
            return;
        }
        if (!this.offer_type_id) {
            this.errors['offer_type_id'] = true;
            return;
        }

        this._offerSubscription = this._offerService.createOffer(
            this.title,
            this.cta,
            this.button_label,
            this.status,
            this.offer_type_id,
            this.offer_category_id,
            1,
            JSON.stringify(this.offer_features)
        ).subscribe((response: any) => {

            if (!response || (response && !response.offer.status)) {
                console.log('error creating offer');
                return;
            }

            jQuery('#addoffermodal').modal('hide');
            this.offers.push(response.offer);
            this._clearModelvalues();

        }, (error: Error) => console.log(error));
    }

    public update(id: number): void {

        this.errors = [];

        if (!this.e_title) {
            this.errors['e_title'] = true;
            return;
        }
        if (!this.e_cta) {
            this.errors['e_cta'] = true;
            return;
        }
        if (!this.e_button_label) {
            this.errors['e_button_label'] = true;
            return;
        }
        if (!this.e_status) {
            this.errors['e_status'] = true;
            return;
        }
        if (!this.e_offer_type_id) {
            this.errors['e_offer_type_id'] = true;
            return;
        }

        this._offerSubscription = this._offerService.updateOffer(
            id,
            this.e_title,
            this.e_cta,
            this.e_button_label,
            this.e_status,
            this.e_offer_type_id,
            this.e_offer_category_id,
            1,
            JSON.stringify(this.offer_features)
        ).subscribe((response: any) => {

            if (!response) {
                console.log('error updating offer');
                return;
            }

            jQuery('#editoffermodal').modal('hide');

            this.offers.forEach((object, index) => {
                if (object.id === id && index !== -1) {
                    this.offers[this.offers.indexOf(object)] = response.offer;
                }
            });

            this._clearModelvalues();

        }, (error: Error) => console.log(error));
    }

    public delete(id: number): void {

        this._offerSubscription = this._offerService.removeOffer(id).subscribe((response: any) => {

            if (!response) {
                return;
            }

            this.offers.forEach((object, index) => {
                if (object.id === id && index !== -1) {
                    this.offers.splice(this.offers.indexOf(object), 1);
                }
            });

            jQuery('#deleteoffermodal').modal('hide');

        }, (error: Error) => console.log(error.message));
    }

    public initDelete(): void {

        jQuery('#editoffermodal').modal('hide');
        jQuery('#deleteoffermodal').modal('show');
    }

    public closeDeleteModal(): void {

        jQuery('#deleteoffermodal').modal('hide');
        jQuery('#editoffermodal').modal('show');
    }

    private _clearModelvalues(): void {

        this.title = '';
        this.cta = '';
        this.button_label = '';
        this.status = null;
        this.offer_type_id = null;
        this.offer_category_id = null;
        this.offer_features = [];

        (<HTMLInputElement>document.getElementById('offer_feature')).value = null;
        (<HTMLInputElement>document.getElementById('offer_benefit')).value = null;
        (<HTMLInputElement>document.getElementById('e_offer_feature')).value = null;
        (<HTMLInputElement>document.getElementById('e_offer_benefit')).value = null;
    }
}
