import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfferCategoryComponent } from './category.component';

const routes: Routes = [
    { path: ':category_id', component: OfferCategoryComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class OfferCategoryRoutingModule {}
