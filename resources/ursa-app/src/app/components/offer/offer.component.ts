import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { OfferService } from './offer.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-offer',
    templateUrl: './offer.component.html'
})

export class OfferComponent implements OnInit, AfterViewInit, OnDestroy {

    public categories: Array<any> = [];

    private _categorySubscription: Subscription;

    constructor(
        private _router: Router,
        private _offerService: OfferService
    ) {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        this._categorySubscription = this._offerService.getCategories().subscribe((response: any) => {

            if (!response) {
                this.categories = [];
                return;
            }

            this.categories = response.categories;

            if (this.categories && this.categories.length > 0) {

                this._router.navigate(['/offers/category', this.categories[0].id]);
            }
        });
    }

    ngOnDestroy() {

        if (this._categorySubscription) {
            this._categorySubscription.unsubscribe();
        }
    }
}
