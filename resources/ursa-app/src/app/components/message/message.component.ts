import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { MessageService } from './message.service';
import { Message, MessageType } from './message.models';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html'
})

export class MessageComponent implements OnInit, AfterViewInit, OnDestroy {

    public types: Array<MessageType> = [];

    private _typeID: number = null;
    private _typeSubscription: Subscription;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _messageService: MessageService
    ) {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        this._route.params.subscribe((params: any) => {
            this._typeID = params.type_id;
        });

        this._typeSubscription = this._messageService.getTypes().subscribe((response: any) => {

            if (!response) {
                this.types = [];
                console.log('error fetching types');
                return;
            }

            this.types = response.types;

            if (this.types.length > 0) {
                this._router.navigate(['/messages/type', this._typeID ? this._typeID : this.types[0].id]);
            }

        }, (error: Error) => console.log(error.message));
    }

    ngOnDestroy() {

        if (this._typeSubscription) {
            this._typeSubscription.unsubscribe();
        }
    }
}
