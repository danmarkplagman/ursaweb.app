import { NgModule } from '@angular/core';

import { MessageComponent } from './message.component';
import { MessageRoutingModule } from './message-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MessageService } from './message.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MessageRoutingModule
    ],
    exports: [
        MessageComponent
    ],
    declarations: [
        MessageComponent
    ],
    providers: [
        MessageService
    ]
})
export class MessageModule { }
