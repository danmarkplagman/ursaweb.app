import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageTypeComponent } from './type.component';

const routes: Routes = [
    { path: ':type_id', component: MessageTypeComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class MessageTypeRoutingModule {}
