import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageTypeHelpModalComponent } from './type-help-modal.component';

@NgModule({
    declarations: [
        MessageTypeHelpModalComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        MessageTypeHelpModalComponent
    ],
    providers: [

    ]
})
export class MessageTypeHelpModalModule {

}
