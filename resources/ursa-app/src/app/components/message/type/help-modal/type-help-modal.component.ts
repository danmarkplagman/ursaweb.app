import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-message-type-help-modal',
    templateUrl: './type-help-modal.component.html'
})
export class MessageTypeHelpModalComponent implements OnInit {

    @Input() typeid: number;

    constructor() {

    }

    ngOnInit(): void {

    }
}

