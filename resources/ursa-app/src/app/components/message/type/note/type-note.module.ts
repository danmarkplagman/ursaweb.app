import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageTypeNoteComponent } from './type-note.component';

@NgModule({
    declarations: [
        MessageTypeNoteComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        MessageTypeNoteComponent
    ],
    providers: [

    ]
})
export class MessageTypeNoteModule {

}
