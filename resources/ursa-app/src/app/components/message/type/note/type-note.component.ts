import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-message-type-note',
    templateUrl: './type-note.component.html'
})

export class MessageTypeNoteComponent implements OnInit {

    @Input() typeid: number;

    constructor() {

    }

    ngOnInit() {

    }
}
