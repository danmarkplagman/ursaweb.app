import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Message, MessageType } from '../message.models';
import { MessageService } from '../message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BroadcasterService } from '../../../shared/broadcaster/broadcaster.service';

declare var jQuery: any;

@Component({
    selector: 'app-message-type',
    templateUrl: './type.component.html'
})

export class MessageTypeComponent implements OnInit, AfterViewInit, OnDestroy {

    public message: Message;
    public max_words: number;
    public is_list: boolean;
    public input_count: number;
    public input_counts: Array<number> = [];
    public messages: Array<any> = [];
    public remaining: number;
    public is_content_loading: boolean;

    /* Model Properties */
    public message_type: number;
    public message_content: string;
    public e_message_content: string;

    private _redirectTimeout: any;
    private _messageSubscription: Subscription;
    private _typeSubscription: Subscription;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _messageService: MessageService,
        private _broadcasterService: BroadcasterService
    ) {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        this._route.params.subscribe((params: any) => {

            this.is_content_loading = true;

            this._messageSubscription = this._messageService.getFirstMessage(1, params.type_id).subscribe((response: any) => {

                this.is_content_loading = false;

                if (!response) {
                    this.message = null;
                    this.message_content = null;
                    return;
                }

                this.is_list = false;
                this.message = response.message;
                this.message_type = this.message.type.id;
                this.message_content = '';
                this.input_count = 0;
                this.input_counts = [];
                this.max_words = this._setMaxWords(this.message_type);

                if (this.message_type === 6 || this.message_type === 7 || this.message_type === 9) {

                    this.is_list = true;
                    this.messages = this.message.messages;
                    this.input_count = this.message.messages.length > 0 ? this.message.messages.length + 1 : 0;
                    this.remaining = 5 - (<number>this.message.messages.length);
                    return;
                }

                this.message_content = this.message.messages[0].message;

                const content = (<any>document.getElementById('message_content'));

                if (this.message_content) {
                    content.value = this.message_content;
                    content.value = content.value.replace(/(^\s*)|(\s*$)/gi, '');
                    content.value = content.value.replace(/[ ]{2,}/gi, ' ');
                    content.value = content.value.replace(/\n /, '\n');
                    this.message.word_count = content.value.split(' ').length;

                    return;
                }

                this.message.word_count =  0;

            }, (error: Error) => console.log(error.message));
        });
    }

    ngOnDestroy() {

        if (this._messageSubscription) {
            this._messageSubscription.unsubscribe();
        }
        if (this._typeSubscription) {
            this._typeSubscription.unsubscribe();
        }
        clearTimeout(this._redirectTimeout);
    }

    public update(): void {

        const messages: Array<any> = [];
        messages.push({
            message: this.message_content
        });

        // tslint:disable-next-line:max-line-length
        this._messageSubscription = this._messageService.updateMessage(this.message.type.id, JSON.stringify(messages), 1).subscribe((response: any) => {

            if (!response || (response && !response.message.status)) {
                console.log('error saving message');
                return;
            }

            if (!response) {
                return;
            }

            jQuery('#editmessagemodal').modal('hide');
            this.message = response.message;
            this._clearModelValues();

        }, (error: Error) => console.log(error.message));
    }

    public updateList(): void {

        const inputs: any = (<any>document.getElementsByName('list_message_content'));
        const messages: Array<any> = [];

        for (let i = 0; i <= inputs.length; i++) {
            if (typeof inputs[i] !== 'undefined') {
                messages.push({
                    message: inputs[i].value
                });
            }
        }

        // tslint:disable-next-line:max-line-length
        this._messageSubscription = this._messageService.updateMessage(this.message.type.id, JSON.stringify(messages), 1).subscribe((response: any) => {

            if (!response || (response && !response.message.status)) {
                console.log('error saving message');
                return;
            }

            if (!response) {
                return;
            }

            jQuery('#editmessagemodal').modal('hide');
            this.message = response.message;
            this._clearModelValues();

        }, (error: Error) => console.log(error.message));
    }

    public updateWordCount(event: any) {

        const content = (<any>document.getElementById('message_content'));
        content.value = this.message_content;

        this.message.word_count = content.value ? content.value.split(' ').length : 0;

        // tslint:disable-next-line:max-line-length
        if (this.message.word_count === this.max_words && (event.which !== 8 && event.which !== 46 && !(event.which >= 37 && event.which <= 40))) {
            event.preventDefault();
        }
    }

    public addRow(): void {

        this.remaining -= 1;
        let count = 0;
        count += 1;

        this.input_counts.push(count);
    }

    public deleteMessage(id: number, index: number): void {

        this._messageSubscription = this._messageService.removeMessage(id).subscribe((response: any) => {

            if (!response) {
                return;
            }

            this.messages.splice(index, 1);
            this.remaining = this.remaining + 1;

        }, (error: Error) => console.log(error.message));
    }

    public deleteInput(index: number): void {

        this.input_counts.splice(index, 1);
        this.remaining = this.remaining + 1;
    }

    private _clearModelValues(): void {

        this.message_type = this.message.type.id;

        if (this.message.type.id !== <number>6 || this.message.type.id !== <number>7 || this.message.type.id !== <number>9) {
            this.messages = [];
            this.messages = this.message.messages;
            this.input_count = this.message.messages.length > 0 ? this.message.messages.length + 1 : 0;
            this.remaining = 5 - (<number>this.message.messages.length);
        }
    }

    private _setMaxWords(message_type_id: number): number {

        let max: number;

        switch (message_type_id) {

            case 1:
                max = 60;
            break;

            case 2:
                max = 50;
            break;

            case 3:
                max = 60;
            break;

            case 4:
                max = 60;
            break;

            case 5:
                max = 50;
            break;

            case 6:
                max = 150;
            break;

            case 7:
                max = 20;
            break;

            case 8:
                max = 200;
            break;

            case 9:
                max = 7;
            break;
        }

        return max;
    }
}
