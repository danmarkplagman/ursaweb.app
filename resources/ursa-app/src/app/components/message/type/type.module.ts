import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MessageTypeRoutingModule } from './type-routing.module';
import { MessageTypeComponent } from './type.component';
import { MessageTypeHelpModalModule } from './help-modal/type-help-modal.module';
import { SortablejsModule } from 'angular-sortablejs/dist';
import { MessageTypeNoteModule } from './note/type-note.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MessageTypeRoutingModule,
        MessageTypeHelpModalModule,
        MessageTypeNoteModule,
        SortablejsModule
    ],
    exports: [
        MessageTypeComponent
    ],
    declarations: [
        MessageTypeComponent
    ],
    providers: [

    ]
})
export class MessageTypeModule { }
