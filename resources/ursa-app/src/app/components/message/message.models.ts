export class Message {

    id: number;
    messages: Array<any>;
    message_type_id: number;
    cmpany_id: number;
    type: MessageType;
    word_count: number;
}

export class MessageType {

    id: number;
    name: string;
    description: string;
}
