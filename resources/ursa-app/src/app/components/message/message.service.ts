import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/do';

@Injectable()
export class MessageService {

    private _prefix = 'message/';

    constructor(
        private _http: HttpClient
    ) {
    }

    public getMessages(company_id: number, type_id: number): Observable<Response> {

        // tslint:disable-next-line:max-line-length
        return this._http.get(environment.API_ENDPOINT + this._prefix + 'lists/' + company_id + '/' + type_id).do((response: any) => response);
    }

    public getFirstMessage(company_id: number, type_id: number): Observable<Response> {

        // tslint:disable-next-line:max-line-length
        return this._http.get(environment.API_ENDPOINT + this._prefix + 'first/' + company_id + '/' + type_id).do((response: any) => response);
    }

    public getTypes(): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'types').do((response: any) => response);
    }

    public getMessageDetails(id: number): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('id', id.toString());

        return this._http.post(environment.API_ENDPOINT + this._prefix + 'details', data.toString()).do((response: any) => response);
    }

    public updateMessage(id: number, message: string, company_id: number): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('message_type_id', id.toString());
        data.append('message', message);
        data.append('company_id', company_id.toString());

        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'update', data.toString()).do((response: any) => response);
    }

    public removeMessage(id: number): Observable<Response> {

        return this._http.delete(environment.API_ENDPOINT + this._prefix + 'delete/' + id).do((response: any) => response);
    }
}
