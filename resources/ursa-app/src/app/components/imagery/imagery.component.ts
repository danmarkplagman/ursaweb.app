import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-imagery',
    templateUrl: './imagery.component.html'
})

export class ImageryComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {

    }
}
