import { NgModule } from '@angular/core';

import { ImageryComponent } from './imagery.component';
import { ImageryRoutingModule } from './imagery-routing.module';
import { ImageryService } from './imagery.service';

@NgModule({
    imports: [
        ImageryRoutingModule
    ],
    exports: [
        ImageryComponent
    ],
    declarations: [
        ImageryComponent
    ],
    providers: [
        ImageryService
    ]
})
export class ImageryModule { }
