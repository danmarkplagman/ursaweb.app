import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageryComponent } from './imagery.component';

const routes: Routes = [
  { path: '', component: ImageryComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class ImageryRoutingModule {}
