import { NgModule } from '@angular/core';

import { PersonaRoutingModule } from './persona-routing.module';
import { PersonaComponent } from './persona.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PersonaHelpModalModule } from './help-modal/persona-help-modal.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PersonaRoutingModule,
        PersonaHelpModalModule
    ],
    exports: [
        PersonaComponent
    ],
    declarations: [
        PersonaComponent
    ],
    providers: [

    ]
})
export class PersonaModule { }
