import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { AudienceService } from '../audience.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Params } from '@angular/router';
import { BroadcasterService } from '../../../shared/broadcaster/broadcaster.service';
import { PersonaDescriptor, PersonaRole } from '../audience.models';

declare var jQuery: any;

interface Persona {
    id: number;
    media_icon_id: number;
    media_icon: string;
    persona_number: number;
    persona_descriptor_id: number;
    persona_role_id: number;
    first_name: string;
    last_name: string;
    description: string;
    demographics: {
        gender: number;
        age: number;
        income: number;
        education: number;
    };
    psychology: {
        needs: number;
        values: number;
        worldview: number;
        technicality: number;
    };
    pain: string;
    desire: string;
    assumptions: string;
    keywords: string;
}

@Component({
    selector: 'app-persona',
    templateUrl: './persona.component.html'
})

export class PersonaComponent implements OnInit, AfterViewInit, OnDestroy {

    public persona: Persona;
    public descriptors: Array<PersonaDescriptor> = [];
    public roles: Array<PersonaRole> = [];
    public errors: Array<any> = [];
    public images: Array<any> = [];
    public has_pain_changes: boolean;
    public has_desire_changes: boolean;
    public has_assumption_changes: boolean;
    public has_keyword_changes: boolean;
    public is_content_loading: boolean;

    /* model properties */
    public e_media_icon: number;
    public e_persona_number: number;
    public e_descriptor_id: number;
    public e_role_id: number;
    public e_description: string;
    public e_gender: number;
    public e_age: number;
    public e_income: number;
    public e_education: number;
    public e_needs: number;
    public e_values: number;
    public e_worldview: number;
    public e_technicality: number;
    public e_pain: string;
    public e_desire: string;
    public e_assumption: string;
    public e_keywords: string;

    private _personaSubscription: Subscription;
    private _tagsTimer: any;

    constructor(
        private _route: ActivatedRoute,
        private _audienceService: AudienceService,
        private _broadcasterService: BroadcasterService
    ) {
        this.has_pain_changes = false;
        this.has_desire_changes = false;
        this.has_assumption_changes = false;
        this.has_keyword_changes = false;
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        this._route.params.subscribe((params: Params) => {

            this.is_content_loading = true;

            this._getPersonaDetails(params.id);
        });

        this._broadcasterService.on('PersonaDataEvent').subscribe((data: any) => {
            this.images = data.images;
            this.descriptors = data.descriptors;
            this.roles = data.roles;
        });
    }

    ngOnDestroy() {

        if (this._personaSubscription) {
            this._personaSubscription.unsubscribe();
        }
    }

    public updatePersona(): void {

        this.errors = [];

        if (!this.e_media_icon) {
            this.errors['e_media_icon'] = true;
            return;
        }
        if (!this.e_descriptor_id) {
            this.errors['e_descriptor_id'] = true;
            return;
        }
        if (!this.e_role_id) {
            this.errors['e_role_id'] = true;
            return;
        }
        if (!this.e_description) {
            this.errors['e_description'] = true;
            return;
        }

        this._personaSubscription = this._audienceService.updatePersona(
            this.persona.id,
            this.e_media_icon,
            this.e_descriptor_id,
            this.e_role_id,
            this.e_description
        ).subscribe((response: any) => {

            if (!response) {
                console.log('error updating');
                return;
            }

            if (response && response.errors) {
                this.errors = response.errors;
                return;
            }

            jQuery('#editpersonamodal').modal('hide');
            this.persona = response.persona;

        }, (error: Error) => {

            console.log(error.message);
        });
    }

    public updateDemographics(): void {

        this._personaSubscription = this._audienceService.updateDemographic(
            this.persona.id,
            this.e_gender,
            this.e_age,
            this.e_income,
            this.e_education
        ).subscribe((response: any) => {

            if (!response || (response && !response.status)) {
                console.log('error updating demographic');
                return;
            }

            jQuery('#editdemographicmodal').modal('hide');

        }, (error: Error) => console.log(error.message));
    }

    public updatePsychology(): void {

        this._personaSubscription = this._audienceService.updatePsychology(
            this.persona.id,
            this.e_needs,
            this.e_values,
            this.e_worldview,
            this.e_technicality
        ).subscribe((response: any) => {

            if (!response || (response && !response.status)) {
                console.log('error updating psychology');
                return;
            }

            jQuery('#editpsychologymodal').modal('hide');

        }, (error: Error) => console.log(error.message));
    }

    public updatePain(): void {

        const e_pain = jQuery('input[name=e_pain]').tagsInput()[0].value;

        this._personaSubscription = this._audienceService.updatePain(this.persona.id, e_pain).subscribe((response: any) => {

            if (!response || (response && !response.status)) {
                console.log('error saving pain');
                return;
            }

            this.has_pain_changes = false;

        }, (error: Error) => console.log(error.message));
    }

    public updateDesire(): void {

        const e_desire = jQuery('input[name=e_desire]').tagsInput()[0].value;

        this._personaSubscription = this._audienceService.updateDesire(this.persona.id, e_desire).subscribe((response: any) => {

            if (!response || (response && !response.status)) {
                console.log('error saving desire');
                return;
            }

            this.has_desire_changes = false;

        }, (error: Error) => console.log(error.message));
    }

    public updateAssumption(): void {

        const e_assumption = jQuery('input[name=e_assumption]').tagsInput()[0].value;

        this._personaSubscription = this._audienceService.updateAssumption(this.persona.id, e_assumption).subscribe((response: any) => {

            if (!response || (response && !response.status)) {
                console.log('error saving assumption');
                return;
            }

            this.has_assumption_changes = false;

        }, (error: Error) => console.log(error.message));
    }

    public updateKeyword(): void {

        const e_keywords = jQuery('input[name=e_keywords]').tagsInput()[0].value;

        this._personaSubscription = this._audienceService.updateKeyword(this.persona.id, e_keywords).subscribe((response: any) => {

            if (!response || (response && !response.status)) {
                console.log('error saving keywords');
                return;
            }

            this.has_keyword_changes = false;

        }, (error: Error) => console.log(error.message));
    }

    public setDeletePersona(event: Event): void {

        event.preventDefault();
        event.stopPropagation();

        jQuery('#editpersonamodal').modal('hide');
        jQuery('#deletepersonamodal').modal('show');
    }

    public delete(id: number): void {

        this._personaSubscription = this._audienceService.deletePersona(id).subscribe((response: any) => {

            if (!response || !response.status) {
                return;
            }

            this._broadcasterService.broadcast('PersonaDeletedEvent', {
                id: id
            });

            jQuery('#deletepersonamodal').modal('hide');

        }, (error: Error) => {

            console.log(error.message);
        });
    }

    public closeDeleteModal(): void {

        jQuery('#deletepersonamodal').modal('hide');
        jQuery('#editpersonamodal').modal('show');
    }

    private _getPersonaDetails(id: number): void {

        this._personaSubscription = this._audienceService.getPersonaDetails(id).subscribe((response: any) => {

            this.is_content_loading = false;

            if (!response) {
                this.persona = null;
                return;
            }

            this.persona = response.persona;
            this.e_media_icon = this.persona.media_icon_id;
            this.e_descriptor_id = this.persona.persona_descriptor_id;
            this.e_role_id = this.persona.persona_role_id;
            this.e_description = this.persona.description;

            this.e_gender = this.persona.demographics.gender;
            this.e_age = this.persona.demographics.age;
            this.e_income = this.persona.demographics.income;
            this.e_education = this.persona.demographics.education;

            this.e_needs = this.persona.psychology.needs;
            this.e_values = this.persona.psychology.values;
            this.e_worldview = this.persona.psychology.worldview;
            this.e_technicality = this.persona.psychology.technicality;

            clearTimeout(this._tagsTimer);
            this._tagsTimer = setTimeout(() => {

                jQuery('input[name=e_pain]').tagsInput({
                    defaultText: '',
                    height: '100px',
                    width: '370px',
                    delimiter: [',', ';', '.'],
                    onAddTag: () => {
                        this.has_pain_changes = true;
                    },
                    onRemoveTag: () => {
                        this.has_pain_changes = true;
                    }
                });

                jQuery('input[name=e_desire]').tagsInput({
                    defaultText: '',
                    height: '100px',
                    width: '370px',
                    delimiter: [',', ';', '.'],
                    onAddTag: () => {
                        this.has_desire_changes = true;
                    },
                    onRemoveTag: () => {
                        this.has_desire_changes = true;
                    }
                });

                jQuery('input[name=e_assumption]').tagsInput({
                    defaultText: '',
                    height: '100px',
                    width: '370px',
                    delimiter: [',', ';', '.'],
                    onAddTag: () => {
                        this.has_assumption_changes = true;
                    },
                    onRemoveTag: () => {
                        this.has_assumption_changes = true;
                    }
                });

                jQuery('input[name=e_keywords]').tagsInput({
                    defaultText: '',
                    height: '100px',
                    width: '370px',
                    delimiter: [',', ';', '.'],
                    onAddTag: () => {
                        this.has_keyword_changes = true;
                    },
                    onRemoveTag: () => {
                        this.has_keyword_changes = true;
                    }
                });

                jQuery('input[name=e_pain]').importTags(this.persona.pain);
                jQuery('input[name=e_desire]').importTags(this.persona.desire);
                jQuery('input[name=e_assumption]').importTags(this.persona.assumptions);
                jQuery('input[name=e_keywords]').importTags(this.persona.keywords);

            }, 100);

        }, (error: Error) => console.log(error.message));
    }
}
