import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonaHelpModalComponent } from './persona-help-modal.component';

@NgModule({
    declarations: [
        PersonaHelpModalComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        PersonaHelpModalComponent
    ],
    providers: [

    ]
})
export class PersonaHelpModalModule {

}
