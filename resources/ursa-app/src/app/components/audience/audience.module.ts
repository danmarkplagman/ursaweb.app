import { NgModule } from '@angular/core';
import { AudienceComponent } from './audience.component';
import { AudienceRoutingModule } from './audience-routing.module';
import { AudienceService } from './audience.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SortablejsModule } from 'angular-sortablejs/dist';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AudienceRoutingModule,
        SortablejsModule
    ],
    exports: [
        AudienceComponent
    ],
    declarations: [
        AudienceComponent
    ],
    providers: [
        AudienceService
    ]
})
export class AudienceModule { }
