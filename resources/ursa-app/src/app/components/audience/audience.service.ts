import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/do';
import { Persona } from './audience.models';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AudienceService {

    private _prefix = 'audience/';

    constructor(
        private _http: HttpClient
    ) {
    }

    public getPersonas(company_id: number): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'lists/' + company_id).do((response: any) => response);
    }

    public getNames(): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + this._prefix + 'names').do((response: any) => response);
    }

    public getPersonaDetails(id: number): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('id', id.toString());

        return this._http.post(environment.API_ENDPOINT + this._prefix + 'details', data.toString()).do((response: any) => response);
    }

    // tslint:disable-next-line:max-line-length
    public createPersona(media_icon: number, persona_number: number, descriptor_id: number, role_id: number, description: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('media_icon', media_icon.toString());
        data.append('persona_number', persona_number.toString());
        data.append('descriptor_id', descriptor_id.toString());
        data.append('role_id', role_id.toString());
        data.append('description', description);

        return this._http.post(environment.API_ENDPOINT + this._prefix + 'create', data.toString()).do((response: any) => response);
    }

    // tslint:disable-next-line:max-line-length
    public updatePersona(id: number, media_icon: number, descriptor_id: number, role_id: number, description: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('id', id.toString());
        data.append('media_icon', media_icon.toString());
        data.append('descriptor_id', descriptor_id.toString());
        data.append('role_id', role_id.toString());
        data.append('description', description);

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'persona/update', data.toString()).do((response: any) => response);
    }

    public updatePersonaNumber(id: number, persona_number: number): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('id', id.toString());
        data.append('persona_number', persona_number.toString());

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'persona-number/update', data.toString()).do((response: any) => response);
    }

    public updateDemographic(persona_id: number, gender: number, age: number, income: number, education: number): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('persona_id', persona_id.toString());
        data.append('gender', gender.toString());
        data.append('age', age.toString());
        data.append('income', income.toString());
        data.append('education', education.toString());

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'demographic/update', data.toString()).do((response: any) => response);
    }

    // tslint:disable-next-line:max-line-length
    public updatePsychology(persona_id: number, needs: number, values: number, worldview: number, technicality: number): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('persona_id', persona_id.toString());
        data.append('needs', needs.toString());
        data.append('values', values.toString());
        data.append('worldview', worldview.toString());
        data.append('technicality', technicality.toString());

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'psychology/update', data.toString()).do((response: any) => response);
    }

    public updatePain(persona_id: number, pain: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('persona_id', persona_id.toString());
        data.append('pain', pain);

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'pain/update', data.toString()).do((response: any) => response);
    }

    public updateDesire(persona_id: number, desire: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('persona_id', persona_id.toString());
        data.append('desire', desire);

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'desire/update', data.toString()).do((response: any) => response);
    }

    public updateAssumption(persona_id: number, assumptions: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('persona_id', persona_id.toString());
        data.append('assumptions', assumptions);

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'assumption/update', data.toString()).do((response: any) => response);
    }

    public updateKeyword(persona_id: number, keywords: string): Observable<Response> {

        const data: URLSearchParams = new URLSearchParams();

        data.append('persona_id', persona_id.toString());
        data.append('keywords', keywords);

        // tslint:disable-next-line:max-line-length
        return this._http.patch(environment.API_ENDPOINT + this._prefix + 'keyword/update', data.toString()).do((response: any) => response);
    }

    public deletePersona(id: number): Observable<Response> {

        return this._http.delete(environment.API_ENDPOINT + this._prefix + 'delete/' + id).do((response: any) => response);
    }

    public getLibraryImages(): Observable<Response> {

        return this._http.get(environment.API_ENDPOINT + 'media-library/images/get').do((response: any) => response);
    }
}
