export class Persona {

    id?: number;
    persona_name_id: number;
    first_name: string;
    last_name: string;
    persona_number: number;
    description: string;
    company_id: number;
    media_library_id: number;
    detail: PersonaDetail;
}

export class PersonaDetail {

    id?: number;
    pain: string;
    desire: string;
    assumptions: string;
    keywords: string;
    persona_id: number;
}

export class PersonaDescriptor {

    id: number;
    name: string;
}

export class PersonaRole {

    id: number;
    name: string;
}
