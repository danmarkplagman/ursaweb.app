import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Persona, PersonaDescriptor, PersonaRole } from './audience.models';
import { Subscription } from 'rxjs/Subscription';
import { AudienceService } from './audience.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BroadcasterService } from '../../shared/broadcaster/broadcaster.service';
import { SortablejsOptions } from 'angular-sortablejs/dist';

declare var jQuery: any;

interface PersonaResponse {
    personas: Array<Persona>;
}

@Component({
    selector: 'app-audience',
    templateUrl: './audience.component.html'
})

export class AudienceComponent implements OnInit, AfterViewInit, OnDestroy {

    public personas: Array<Persona> = [];
    public descriptors: Array<PersonaDescriptor> = [];
    public roles: Array<PersonaRole> = [];
    public images: Array<any> = [];
    public is_loading: boolean;
    public errors: Array<any> = [];
    public persona_id: number;
    public options: SortablejsOptions;

    /* model properties */
    public media_icon: number;
    public persona_number: number;
    public descriptor_id: number;
    public role_id: number;
    public description: string;

    private _personaSubscription: Subscription;
    private _namesSubscription: Subscription;
    private _mediaLibrarySubscription: Subscription;
    private _broadcasterSubscription: Subscription;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _audienceService: AudienceService,
        private _broadcasterService: BroadcasterService
    ) {
        this.media_icon = 1;
        this.persona_number = 0;
        this.descriptor_id = null;
        this.role_id = null;
        this.description = '';

        this.options = {

            onUpdate: (event: any) => {

                if (event.newIndex !== event.oldIndex) {

                    // tslint:disable-next-line:max-line-length
                    this._personaSubscription = this._audienceService.updatePersonaNumber(event.srcElement.children[event.oldIndex].dataset.personaid, event.newIndex).subscribe((data: any) => {

                        if (!data || (data && !data.status)) {
                            return;
                        }

                    }, (error: Error) => console.log(error.message));
                }
            }
        };
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        jQuery('.tags').tagsInput({
            defaultText: '',
            width: '100%',
            delimiter: [',', ';', '.']
        });

        this._getPersonas();
        this._getNames();
        this._getImages();

        // tslint:disable-next-line:max-line-length
        this._broadcasterSubscription = this._broadcasterService.on('PersonaDeletedEvent').subscribe((data: any) => this._removeDeletedPersona(data.id));
    }

    ngOnDestroy() {

        if (this._personaSubscription) {
            this._personaSubscription.unsubscribe();
        }
        if (this._namesSubscription) {
            this._namesSubscription.unsubscribe();
        }
        if (this._mediaLibrarySubscription) {
            this._mediaLibrarySubscription.unsubscribe();
        }
    }

    public create(): void {

        this.errors = [];

        if (!this.media_icon) {
            this.errors['media_icon'] = true;
            return;
        }
        if (!this.descriptor_id) {
            this.errors['descriptor_id'] = true;
            return;
        }
        if (!this.role_id) {
            this.errors['role_id'] = true;
            return;
        }
        if (!this.description) {
            this.errors['description'] = true;
            return;
        }

        this._personaSubscription = this._audienceService.createPersona(
            this.media_icon,
            this.persona_number,
            this.descriptor_id,
            this.role_id,
            this.description
        ).subscribe((response: any) => {

            if (!response) {
                console.log('error saving');
                return;
            }

            if (response && response.errors) {
                this.errors = response.errors;
                return;
            }

            if (response && response.max_persona) {
                alert('There are 5 Personas already created!');
                return;
            }

            jQuery('#addpersonamodal').modal('hide');
            this._clearModelValues();
            this.personas.push(response.persona);
            this._router.navigate(['/audience/persona', response.persona.id]);

        }, (error: Error) => {

            console.log(error.message);
        });
    }

    private _getPersonas(): void {

        this.is_loading = true;

        this._personaSubscription = this._audienceService.getPersonas(1).subscribe((response: any) => {

            this.is_loading = false;

            if (!response) {
                this.personas = [];
                return;
            }

            if (response && response.personas.length > 0) {
                this.personas = response.personas;
                this.persona_number = Number(this.personas.length);

                if (this._route.children.length <= 0) {
                    this._router.navigate(['/audience/persona', this.personas[0].id]);
                }
            }

        }, (error: Error) => {

            this.is_loading = false;
            console.log(error.message);
        });
    }

    private _getNames(): void {

        this._namesSubscription = this._audienceService.getNames().subscribe((response: any) => {

            if (!response) {
                this.descriptors = [];
                this.roles = [];
                return;
            }

            if (response && response.names.descriptors.length > 0) {
                this.descriptors = response.names.descriptors;
            }

            if (response && response.names.roles.length > 0) {
                this.roles = response.names.roles;
            }

        }, (error: Error) => console.log(error.message));
    }

    private _getImages(): void {

        this._mediaLibrarySubscription = this._audienceService.getLibraryImages().subscribe((response: any) => {

            if (!response) {
                this.images = [];
                return;
            }

            this.images = response.images;

            setTimeout(() => {
                this._broadcasterService.broadcast('PersonaDataEvent', {
                    images: this.images,
                    descriptors: this.descriptors,
                    roles: this.roles
                });
            }, 1000);

        }, (error: Error) => {

            console.log(error.message);
        });
    }

    private _removeDeletedPersona(id: number): void {

        this.personas.forEach((item, index) => {
            if (item.id === id) {
                this.personas.splice(index, 1);
            }
        });
        jQuery('#deletepersonamodal').modal('hide');
        this._router.navigate(['/audience/persona', this.personas[0].id]);
    }

    private _clearModelValues(): void {

        this.media_icon = 1;
        this.persona_number = this.personas.length;
        this.descriptor_id = null;
        this.role_id = null;
        this.description = '';
    }
}
