import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

interface BroadcastEvent {
    key: any;
    data?: any;
}

export class BroadcasterService {

    private _eventBus: Subject<BroadcastEvent>;
    private _timeout: any;

    constructor() {

        this._eventBus = new Subject<BroadcastEvent>();
    }

    broadcast(key: any, data?: any) {

        clearInterval(this._timeout);
        this._timeout = setTimeout(() => {
            this._eventBus.next({key, data});
        }, 100);
    }

    on<T>(key: any): Observable<T> {

        return this._eventBus.asObservable()
            .filter((event: BroadcastEvent) => event.key === key)
            .map(event => <T>event.data);
    }
}
