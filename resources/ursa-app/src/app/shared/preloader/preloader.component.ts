import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BroadcasterService } from '../broadcaster/broadcaster.service';

interface PageloadInterface {
    isloading: boolean;
}

@Component({
    selector: 'app-preloader',
    templateUrl: './preloader.component.html'
})

export class PreloaderComponent implements OnInit, AfterViewInit, OnDestroy {

    public shown: boolean;

    private _pageloadSubscription: Subscription;

    constructor(
        private _broadcaster: BroadcasterService
    ) {
        this.shown = true;
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        if (this._pageloadSubscription) {
            this._pageloadSubscription.unsubscribe();
        }
        this._pageloadSubscription = this._broadcaster.on('PageloadEvent').subscribe((data: PageloadInterface) => {
            this.shown = data.isloading;
        });
    }

    ngOnDestroy() {

        if (this._pageloadSubscription) {
            this._pageloadSubscription.unsubscribe();
        }
    }
}
