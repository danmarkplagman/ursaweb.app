import { NgModule } from '@angular/core';
import { PreloaderComponent } from './preloader.component';
import { PreloaderService } from './preloader.service';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        PreloaderComponent
    ],
    declarations: [
        PreloaderComponent
    ],
    providers: [
        PreloaderService
    ]
})
export class PreloaderModule { }
