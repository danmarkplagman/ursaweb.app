import { Injectable } from '@angular/core';
import { BroadcasterService } from '../broadcaster/broadcaster.service';

@Injectable()
export class PreloaderService {

    constructor(
        private _broadcaster: BroadcasterService
    ) {

    }

    show(): void {

        this._broadcaster.broadcast('PageloadEvent', {
            isloading: true
        });
    }

    hide(): void {

        this._broadcaster.broadcast('PageloadEvent', {
            isloading: false
        });
    }
}
