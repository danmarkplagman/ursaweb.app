import { NgModule } from '@angular/core';
import { PreloaderModule } from './preloader/preloader.module';
import { BroadcasterService } from './broadcaster/broadcaster.service';

@NgModule({
    imports: [
        PreloaderModule
    ],
    exports: [
        PreloaderModule
    ],
    declarations: [

    ],
    providers: [
        BroadcasterService
    ]
})
export class SharedModule { }
