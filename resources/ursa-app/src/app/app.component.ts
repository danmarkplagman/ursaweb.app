import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { PreloaderService } from './shared/preloader/preloader.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

    private _routerSubscription: Subscription;

    constructor(
        private _router: Router,
        private _preloaderService: PreloaderService
    ) {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        if (this._routerSubscription) {
            this._routerSubscription.unsubscribe();
        }
        this._routerSubscription = this._router.events.subscribe((event: any) => {

            if (event instanceof NavigationStart) {

                this._preloaderService.show();

            } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {

                this._preloaderService.hide();
            }
        });
    }

    ngOnDestroy() {

        if (this._routerSubscription) {
            this._routerSubscription.unsubscribe();
        }
    }
}
