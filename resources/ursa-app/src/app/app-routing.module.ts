import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AudienceComponent } from './components/audience/audience.component';

const routes: Routes = [
    { path: '', children: [
        { path: '', redirectTo: 'audience', pathMatch: 'full' },
        { path: 'audience', loadChildren: './components/audience/audience.module#AudienceModule' },
        { path: 'messages', loadChildren: './components/message/message.module#MessageModule' },
        { path: 'offers', loadChildren: './components/offer/offer.module#OfferModule' },
        { path: 'imagery', loadChildren: './components/imagery/imagery.module#ImageryModule' },
        { path: 'seeds', loadChildren: './components/seed/seed.module#SeedModule' },
        { path: 'associations', loadChildren: './components/association/association.module#AssociationModule' },
        { path: 'credentials', loadChildren: './components/credential/credential.module#CredentialModule' }
    ] },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule {}
