import { Injectable } from '@angular/core';
import { Cookie } from 'ng2-cookies';

@Injectable()
export class AuthService {

    public getToken(): string {

        return Cookie.get('tkn');
    }

    public isAuthenticated(): boolean {

        const token = this.getToken();

        return true;
    }
}
