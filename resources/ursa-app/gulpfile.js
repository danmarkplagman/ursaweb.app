const gulp = require('gulp');
const rename = require('gulp-rename');
const del = require('del');
const cleanCSS = require('gulp-clean-css');

var paths = {
    dist: '../../resources/app/dist/**'
};

gulp.task('default', function() {

    // delete all old files
    gulp.start('clean');

    // copy new files
    gulp.src(['dist/**', '!dist/*.html'])
        .pipe(gulp.dest('../../public/'));

    // copy the html to generated index php
    gulp.src('dist/index.html')
         .pipe(rename('index.blade.php'))
         .pipe(gulp.dest('../../resources/views/home/'));

    gulp.start('css');
});

gulp.task('clean', function(){

    del.sync([
        '../../public/*.*',
        '!../../public/index.php',
        '!../../public/assets',
        '!../../public/medias',
        '!../../public/mix-manifest.json',
        '!../../public/.htaccess',
        '!../../public/robots.txt'
    ],
    {force : true});
});

gulp.task('css', function() {

    var styles = [
        
    ];

    return gulp.src(styles)
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('../../public/assets/css'));
});

gulp.task('watch', function() {

    gulp.watch(paths.dist, ['default']);
});
