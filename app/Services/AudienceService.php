<?php
/*
* Service Class for Audience
*
*/

namespace App\Services;

use App\Persona;
use App\Demographic;
use App\PersonaDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\AudienceServiceInterface;

class AudienceService implements AudienceServiceInterface
{
    /*
    * GET
    * Get Persona Lists
    * Params: company_id
    */
    public function lists(Request $request, $company_id)
    {
        $personas = Persona::getByCompanyId($company_id);

        return compact('personas');
    }

    /*
    * GET
    * Get Persona Names
    * Params: 
    */
    public function personaNames(Request $request)
    {
        $names = Persona::getNames();

        return compact('names');
    }

    /*
    * POST
    * Get Persona Details
    * Params: id
    */
    public function details(Request $request)
    {
        $persona = Persona::getDetails($request->id);

        return compact('persona');
    }

    /*
    * POST
    * Create Persona
    * Params: body
    */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'media_icon'    => 'required',
            'descriptor_id' => 'required',
            'role_id'       => 'required',
            'description'   => 'required'
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        $persona = Persona::createInitial($request);

        return compact('persona');
    }

    /*
    * PATCH
    * Update Persona Details
    * Params: body
    */
    public function updatePersona(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'media_icon'    => 'required',
            'descriptor_id' => 'required',
            'role_id'       => 'required',
            'description'   => 'required'
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        $persona = Persona::updatePersona($request);

        return compact('persona');
    }

    /*
    * PATCH
    * Update Persona Number
    * Params: body
    */
    public function updatePersonaNumber(Request $request)
    {
        return Persona::updatePersonaNumber($request);
    }

    /*
    * PATCH
    * Update Persona Demographics
    * Params: body
    */
    public function updateDemographic(Request $request)
    {
        return Persona::updateDemographic($request);
    }

    /*
    * PATCH
    * Update Persona Psychology
    * Params: body
    */
    public function updatePsychology(Request $request)
    {
        return Persona::updatePsychology($request);
    }

    /*
    * PATCH
    * Update Persona Pain
    * Params: body
    */
    public function updatePain(Request $request)
    {
        return Persona::updatePain($request);
    }

    /*
    * PATCH
    * Update Persona Desire
    * Params: body
    */
    public function updateDesire(Request $request)
    {
        return Persona::updateDesire($request);
    }

    /*
    * PATCH
    * Update Persona Assumptions
    * Params: body
    */
    public function updateAssumption(Request $request)
    {
        return Persona::updateAssumption($request);
    }

    /*
    * PATCH
    * Update Persona Keywords
    * Params: body
    */
    public function updateKeyword(Request $request)
    {
        return Persona::updateKeyword($request);
    }

    /*
    * GET
    * Delete Persona
    * Params: id
    */
    public function delete(Request $request, $id)
    {
        $delete = Persona::remove($id);

        return ['status' => $delete];
    }
}