<?php
/*
* Interface Class for Messages
*
*/

namespace App\Services;

use Illuminate\Http\Request;

interface MessageServiceInterface 
{
    /*
    * GET
    * Get Messages List
    * Params: company_id, type_id
    */
    public function lists(Request $request, $company_id, $type_id);

    /*
    * GET
    * Get First Message
    * Params: company_id, type_id
    */
    public function first(Request $request, $company_id, $type_id);

    /*
    * GET
    * Get Message Types
    * 
    */
    public function types(Request $request);

    /*
    * POST
    * Get Message Details
    * Params: id
    */
    public function details(Request $request);

    /*
    * PATCH
    * Update Message
    * Params: body
    */
    public function update(Request $request);

    /*
    * DELETE
    * Delete Message
    * Params: id
    */
    public function delete(Request $request, $id);
}