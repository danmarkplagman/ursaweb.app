<?php
/*
* Interface Class for Media Library
*
*/

namespace App\Services;

use Illuminate\Http\Request;

interface MediaLibraryServiceInterface 
{
    /*
    * Get Library Images
    *
    */
    public function getImages(Request $request);
}