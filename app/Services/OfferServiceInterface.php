<?php
/*
* Interface Class for Offers
*
*/

namespace App\Services;

use Illuminate\Http\Request;

interface OfferServiceInterface 
{
    /*
    * GET
    * Get Offers List
    * Params: company_id, feature_id
    */
    public function lists(Request $request, $company_id, $feature_id);

    /*
    * GET
    * Get Offers Types
    * Params:
    */
    public function types(Request $request);

    /*
    * GET
    * Get Offers Categories
    * Params:
    */
    public function categories(Request $request);

    /*
    * GET
    * Get Offer Features
    * Params: 
    */
    public function features(Request $request);

    /*
    * GET
    * Get Offer Details
    * Params: id
    */
    public function details(Request $request, $id);

    /*
    * POST
    * Create Offer
    * Params: body
    */
    public function create(Request $request);

    /*
    * PATCH
    * Update Offer
    * Params: body
    */
    public function update(Request $request);

    /*
    * DELETE
    * Delete Offer
    * Params: id
    */
    public function delete(Request $request, $id);
}