<?php
namespace App\Services;

use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    /*
    * Register Service Providers
    *
    */
    public function register()
    {
        $this->app->bind(AudienceServiceInterface::class, AudienceService::class);
        $this->app->bind(MediaLibraryServiceInterface::class, MediaLibraryService::class);
        $this->app->bind(MessageServiceInterface::class, MessageService::class);
        $this->app->bind(OfferServiceInterface::class, OfferService::class);
    }
}