<?php
/*
* Service Class for Offers
*
*/

namespace App\Services;

use App\Offer;
use App\OfferType;
use App\OfferFeature;
use App\OfferCategory;
use Illuminate\Http\Request;
use App\Services\OfferServiceInterface;
use Illuminate\Support\Facades\Validator;

class OfferService implements OfferServiceInterface
{
    /*
    * GET
    * Get Offers List
    * Params: company_id, feature_id
    */
    public function lists(Request $request, $company_id, $category_id)
    {
        $offers = Offer::getOffers($company_id, $category_id);

        return compact('offers');
    }

    /*
    * GET
    * Get Offers Types
    * Params:
    */
    public function types(Request $request)
    {
        $types = OfferType::getTypes();

        return compact('types');
    }

    /*
    * GET
    * Get Offers Categories
    * Params:
    */
    public function categories(Request $request)
    {
        $categories = OfferCategory::getCategories();

        return compact('categories');
    }

    /*
    * GET
    * Get Offer Features
    * Params: 
    */
    public function features(Request $request)
    {
        $features = Offer::getFeatures();
        
        return compact('features');
    }

    /*
    * GET
    * Get Offer Details
    * Params: id
    */
    public function details(Request $request, $id)
    {
        $offer = Offer::getDetails($id);

        return compact('offer');
    }

    /*
    * POST
    * Create Offer
    * Params: body
    */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'cta'           => 'required',
            'button_label'  => 'required',
            'offer_type_id' => 'required',
            'status'        => 'required'
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        $offer = Offer::createOffer($request);

        return compact('offer');
    }

    /*
    * PATCH
    * Update Offer
    * Params: body
    */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required',
            'cta'           => 'required',
            'button_label'  => 'required',
            'offer_type_id' => 'required',
            'status'        => 'required'
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        $offer = Offer::updateOffer($request);

        return compact('offer');
    }

    /*
    * DELETE
    * Delete Offer
    * Params: id
    */
    public function delete(Request $request, $id)
    {
        return Offer::removeOffer($id);
    }
}
