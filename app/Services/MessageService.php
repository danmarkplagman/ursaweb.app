<?php
/*
* Service Class for Messages
*
*/

namespace App\Services;

use App\Message;
use App\MessageType;
use App\MediaLibrary;
use Illuminate\Http\Request;
use App\Services\MessageServiceInterface;
use Illuminate\Support\Facades\Validator;

class MessageService implements MessageServiceInterface
{
    /*
    * GET
    * Get Messages List
    * Params: company_id, type_id
    */
    public function lists(Request $request, $company_id, $type_id)
    {
        $messages = Message::getLists($company_id, $type_id);

        return compact('messages');
    }

    /*
    * GET
    * Get First Message
    * Params: company_id, type_id
    */
    public function first(Request $request, $company_id, $type_id)
    {
        $message = Message::getFirst($company_id, $type_id);

        return compact('message');
    }

    /*
    * GET
    * Get Message Types
    * 
    */
    public function types(Request $request)
    {
        $types = MessageType::get();

        return compact('types');
    }

    /*
    * POST
    * Get Message Details
    * Params: id
    */
    public function details(Request $request)
    {
        $message = Message::getDetails($request->id);

        return compact('message');
    }

    /*
    * PATCH
    * Update Message
    * Params: body
    */
    public function update(Request $request)
    {
        $message = Message::createOrUpdateMessage($request);

        return compact('message');
    }

    /*
    * DELETE
    * Delete Message
    * Params: id
    */
    public function delete(Request $request, $id)
    {
        $message = Message::removeMessage($id);

        return compact('message');
    }
}