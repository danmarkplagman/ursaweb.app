<?php
/*
* Interface Class for Audience
*
*/

namespace App\Services;

use Illuminate\Http\Request;

interface AudienceServiceInterface 
{
    /*
    * GET
    * Get Persona Lists
    * Params: company_id
    */
    public function lists(Request $request, $company_id);

    /*
    * GET
    * Get Persona Names
    * Params: 
    */
    public function personaNames(Request $request);

    /*
    * POST
    * Get Persona Details
    * Params: id
    */
    public function details(Request $request);

    /*
    * POST
    * Create Persona
    * Params: body
    */
    public function create(Request $request);

    /*
    * PATCH
    * Update Persona Details
    * Params: body
    */
    public function updatePersona(Request $request);

    /*
    * PATCH
    * Update Persona Number
    * Params: body
    */
    public function updatePersonaNumber(Request $request);

    /*
    * PATCH
    * Update Persona Demographics
    * Params: body
    */
    public function updateDemographic(Request $request);

    /*
    * PATCH
    * Update Persona Psychology
    * Params: body
    */
    public function updatePsychology(Request $request);

    /*
    * PATCH
    * Update Persona Pain
    * Params: body
    */
    public function updatePain(Request $request);

    /*
    * PATCH
    * Update Persona Desire
    * Params: body
    */
    public function updateDesire(Request $request);

    /*
    * PATCH
    * Update Persona Assumptions
    * Params: body
    */
    public function updateAssumption(Request $request);

    /*
    * PATCH
    * Update Persona Keywords
    * Params: body
    */
    public function updateKeyword(Request $request);

    /*
    * DELETE
    * Delete Persona
    * Params: id
    */
    public function delete(Request $request, $id);
}