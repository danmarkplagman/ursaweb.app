<?php
/*
* Service Class for Media Library
*
*/

namespace App\Services;

use App\MediaLibrary;
use Illuminate\Http\Request;
use App\Services\MediaLibraryServiceInterface;

class MediaLibraryService implements MediaLibraryServiceInterface
{
    /*
    * Get Library Images
    *
    */
    public function getImages(Request $request)
    {
        $lists = MediaLibrary::get();
        $images = [];

        foreach ($lists as $list) {

            $images[] = [
                'id'    => $list->id,
                'image' => '//bsf.local/medias/' . $list->image_file_name
            ];
        }

        return compact('images');
    }
}