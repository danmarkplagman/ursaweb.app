<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferType extends Model
{
    protected $fillable = [
        'name',
        'description',
        'media_library_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*
    * Model Relationships
    *
    */

    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }

    public function media()
    {
        return $this->hasOne('App\MediaLibrary', 'id', 'media_library_id');
    }

    /*
    * Custom Methods
    *
    */

    public static function getTypes()
    {
        return self::get();
    }
}
