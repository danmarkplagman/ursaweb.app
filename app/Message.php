<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'message',
        'message_type_id',
        'company_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*
    * Model Relationships
    *
    */

    public function type()
    {
        return $this->hasOne('App\MessageType', 'id', 'message_type_id');
    }

    /*
    * Custom Methods
    *
    */

    public static function getLists($company_id, $type_id)
    {
        return Message::where([
            'company_id'        => $company_id,
            'message_type_id'   => $type_id
        ])
        ->get();
    }

    public static function getFirst($company_id, $type_id)
    {
        $messages = [];
        $type = MessageType::find($type_id);

        return $type ? [
            'id'        => $type->id,
            'messages'  => self::getDetails($company_id, $type_id),
            'type'      => [
                'id'    => $type->id,
                'name'  => $type->name
            ],
            'status'    => true
        ] : [];
    }

    public static function getDetails($company_id, $type_id)
    {
        $message = [];
        $messages = [];
        $lists = Message::where([
            'message_type_id'   => $type_id,
            'company_id'        => $company_id
        ])
        ->get();

        foreach ($lists as $list) {

            $messages[] = [
                'id'        => $list->id,
                'message'   => (string)$list->message
            ];
        }

        return $lists->count() > 0 ? $messages : [];
    }

    public static function createOrUpdateMessage($request)
    {
        $msgs = json_decode($request->message);

        if ((count($msgs)) > 0) {

            Message::where([
                'message_type_id'   => $request->message_type_id,
                'company_id'        => $request->company_id
            ])
            ->delete();

            foreach ($msgs as $msg) {

                $update = Message::create([
                    'message_type_id'   => $request->message_type_id,
                    'company_id'        => $request->company_id,
                    'message'           => $msg->message
                ]);
            }
        }

        $message = Message::find($update->id);

        return $message ? [
            'status'    => true,
            'id'        => $message->id,
            'messages'  => self::getDetails($request->company_id, $request->message_type_id),
            'type'      => [
                'id'    => $message->type->id,
                'name'  => $message->type->name
            ]
        ] : [
            'status' => false
        ];
    }

    public static function removeMessage($id)
    {
        Message::where([
            'id' => $id
        ])
        ->delete();

        return ['status' => true];
    }
}

