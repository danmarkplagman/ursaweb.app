<?php

namespace App;

use App\OfferFeature;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'title',
        'cta',
        'button_label',
        'status',
        'offer_type_id',
        'offer_category_id',
        'company_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*
    * Model Relationships
    *
    */

    public function category()
    {
        return $this->hasOne('App\OfferCategory');
    }

    public function type()
    {
        return $this->hasOne('App\OfferType', 'id', 'offer_type_id');
    }

    public function features()
    {
        return $this->hasMany('App\OfferFeature');
    }

    public function media()
    {
        return $this->hasOne('App\MediaLibrary');
    }

    /*
    * Custom Methods
    *
    */

    public static function getOffers($company_id, $category_id)
    {
        $offers = [];
        $lists = self::where([
            'company_id'        => $company_id,
            'offer_category_id' => $category_id
        ])
        ->get();

        foreach ($lists as $list) {

            $offers[] = [
                'id'                => $list->id,
                'title'             => $list->title,
                'cta'               => $list->cta,
                'button_label'      => $list->button_label,
                'status'            => $list->status,
                'offer_type_id'     => $list->offer_type_id,
                'offer_category_id' => $list->offer_category_id,
                'media_icon'        => '//bsf.local/medias/' . $list->type->media->image_file_name,
                'offer_type_name'   => $list->type->name,
                'company_id'        => $list->company_id,
                'features'          => $list->features,
                'show_feature'      => false
            ];
        }

        return $offers;
    }

    public static function getDetails($id)
    {
        $offer = self::with('features')->find($id);

        return $offer;
    }

    public static function getFeatures()
    {
        $features = [];
        $lists = OfferFeature::get();

        foreach ($lists as $list) {

            $features[] = [
                'id'        => $list->id,
                'feature'   => $list->feature,
                'benefit'   => $list->benefit,
                'offers'    => $list->offers
            ];
        }

        return $features;
    }

    public static function createOffer($request)
    {
        $features = json_decode($request->features);

        $create = self::create([
            'title'             => $request->title,
            'cta'               => $request->cta,
            'button_label'      => $request->button_label,
            'status'            => $request->status,
            'offer_type_id'     => $request->offer_type_id,
            'offer_category_id' => $request->offer_category_id,
            'company_id'        => $request->company_id
        ]);

        if ($create && (count($features)) > 0) {

            foreach ($features as $feature) {

                OfferFeature::create([
                    'feature'   => $feature->feature,
                    'benefits'  => $feature->benefits,
                    'offer_id'  => $create->id 
                ]);
            }
        }

        $offer = self::with('features')->find($create->id);

        return $create ? [
            'id'                => $offer->id,
            'title'             => $offer->title,
            'cta'               => $offer->cta,
            'button_label'      => $offer->button_label,
            'status'            => $offer->status,
            'offer_type_id'     => $offer->offer_type_id,
            'offer_category_id' => $offer->offer_category_id,
            'company_id'        => $offer->company_id,
            'features'          => $offer->features,
            'media_icon'        => '//bsf.local/medias/' . $offer->type->media->image_file_name,
            'offer_type_name'   => $offer->type->name,
            'status'            => true
        ] : [
            'status' => false
        ];
    }

    public static function updateOffer($request)
    {
        $features = json_decode($request->features);

        $update = self::where([
            'id' => $request->id
        ])
        ->update([
            'title'             => $request->title,
            'cta'               => $request->cta,
            'button_label'      => $request->button_label,
            'status'            => $request->status,
            'offer_type_id'     => $request->offer_type_id,
            'offer_category_id' => $request->offer_category_id,
            'company_id'        => $request->company_id
        ]);

        OfferFeature::where([
            'offer_id' => $request->id
        ])
        ->delete();

        if ($update && (count($features)) > 0) {

            foreach ($features as $feature) {

                OfferFeature::create([
                    'feature'   => $feature->feature,
                    'benefits'  => $feature->benefits,
                    'offer_id'  => $request->id 
                ]);
            }
        }

        $offer = self::with('features')->find($request->id);

        return $update ? [
            'id'                => $offer->id,
            'title'             => $offer->title,
            'cta'               => $offer->cta,
            'button_label'      => $offer->button_label,
            'status'            => $offer->status,
            'offer_type_id'     => $offer->offer_type_id,
            'offer_category_id' => $offer->offer_category_id,
            'company_id'        => $offer->company_id,
            'features'          => $offer->features,
            'media_icon'        => '//bsf.local/medias/' . $offer->type->media->image_file_name,
            'offer_type_name'   => $offer->type->name
        ] : [
            'status' => false
        ];
    }

    public static function removeOffer($id)
    {
        $offer = self::where([
            'id' => $id
        ])
        ->delete();

        if ($offer) {

            OfferFeature::where([
                'offer_id' => $id
            ])
            ->delete();
        }

        return ['status' => true];
    }
}
