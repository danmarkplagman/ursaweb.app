<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CredentialType extends Model
{
    protected $fillable = [
        'name',
        'description',
        'media_library_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
