<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociationType extends Model
{
    protected $fillable = [
        'name',
        'description',
        'association_category_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
