<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferCategory extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*
    * Model Relationships
    *
    */

    public function offer()
    {
        return $this->hasOne('App\Offer');
    }

    /*
    * Custom Methods
    *
    */

    public static function getCategories()
    {
        return self::get();
    }
}
