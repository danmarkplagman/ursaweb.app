<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
    protected $fillable = [
        'title',
        'content',
        'url',
        'credential_type_id',
        'company_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
