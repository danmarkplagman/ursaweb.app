<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name', 
        'last_name',
        'email',
        'contact_number',
        'status',
        'media_library_id'
    ];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at'
    ];
}
