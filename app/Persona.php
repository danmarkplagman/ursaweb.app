<?php

namespace App;

use App\Psychology;
use App\Demographic;
use App\PersonaRole;
use App\PersonaDetail;
use App\PersonaDescriptor;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = [
        'persona_number',
        'description',
        'company_id',
        'media_library_id',
        'persona_descriptor_id',
        'persona_role_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*
    * Model Relationships
    *
    */

    public function descriptor()
    {
        return $this->hasOne('App\PersonaDescriptor', 'id', 'persona_descriptor_id');
    }

    public function role()
    {
        return $this->hasOne('App\PersonaRole', 'id', 'persona_role_id');
    }

    public function details()
    {
        return $this->hasOne('App\PersonaDetail');
    }

    public function media()
    {
        return $this->hasOne('App\MediaLibrary', 'id', 'media_library_id');
    }

    public function demographics()
    {
        return $this->hasOne('App\Demographic');
    }

    public function psychology()
    {
        return $this->hasOne('App\Psychology');
    }

    /*
    * Custom Methods
    *
    */

    public static function getByCompanyId($company_id)
    {
        $lists = self::where([
            'company_id' => $company_id
        ])
        ->orderBy('persona_number', 'asc')
        ->get();

        $personas = [];

        foreach ($lists as $list) {

            $personas[] = [
                'id'    => $list->id,
                'persona_number'        => (int)$list->persona_number,
                'desciption'            => $list->description,
                'company_id'            => (int)$list->company_id,
                'persona_descriptor_id' => $list->persona_descriptor_id,
                'persona_role_id'       => (int)$list->persona_role_id,
                'first_name'            => $list->descriptor->name,
                'last_name'             => $list->role->name
            ];
        }

        return $personas;
    }

    public static function getNames()
    {
        $descriptors    = PersonaDescriptor::get();
        $roles          = PersonaRole::get();

        return [
            'descriptors'   => $descriptors,
            'roles'         => $roles
        ];
    }

    public static function getDetails($id)
    {
        $data = self::where('id', $id)
            ->first();

        $persona = [
            'id'                    => (int)$data->id,
            'media_icon_id'         => (int)$data->media->id,
            'media_icon'            => '//bsf.local/medias/' . $data->media->image_file_name,
            'persona_descriptor_id' => $data->descriptor->name,
            'persona_role_id'       => $data->role->name,
            'persona_number'        => (int)$data->persona_number,
            'first_name'            => ucwords($data->descriptor->name),
            'last_name'             => ucwords($data->role->name),
            'description'           => $data->description,
            'demographics'          => [
                'gender'            => (int)$data->demographics->gender,
                'age'               => (int)$data->demographics->age,  
                'income'            => (int)$data->demographics->income,
                'education'         => (int)$data->demographics->education
            ],
            'psychology'            => [
                'needs'             => (int)$data->psychology->needs,
                'values'            => (int)$data->psychology->values,
                'worldview'         => (int)$data->psychology->worldview,
                'technicality'      => (int)$data->psychology->technicality
            ],
            'pain'                  => (string)$data->details->pain,
            'desire'                => (string)$data->details->desire,
            'assumptions'           => (string)$data->details->assumptions,
            'keywords'              => (string)$data->details->keywords
        ];

        return $persona;
    }

    public static function createInitial($request)
    {
        $check = self::get();

        if ($check->count() === 5) {
            return [
                'status'        => true,
                'max_persona'   => true
            ];
        }

        $persona = self::create([
            'persona_number'        => $request->persona_number,
            'description'           => $request->description,
            'company_id'            => 1,
            'media_library_id'      => $request->media_icon,
            'persona_descriptor_id' => $request->descriptor_id,
            'persona_role_id'       => $request->role_id
        ]);

        $personaDetail = PersonaDetail::create([
            'persona_id'    => $persona->id
        ]);

        $demographics = Demographic::create([
            'gender'        => 0,
            'age'           => 0,
            'income'        => 0,
            'education'     => 0,
            'persona_id'    => $persona->id
        ]);

        $psychology = Psychology::create([
            'needs'         => 0,
            'values'        => 0,
            'worldview'     => 0,
            'technicality'  => 0,
            'persona_id'    => $persona->id
        ]);

        return $persona ? [
            'status'        => true,
            'id'            => $persona->id,
            'first_name'    => ucwords($persona->descriptor->name),
            'last_name'     => ucwords($persona->role->name)
        ] : [
            'status' => false
        ];
    }

    public static function updatePersona($request)
    {
        $update = self::where('id', $request->id)
        ->update([
            'media_library_id'      => $request->media_icon,
            'persona_descriptor_id' => $request->descriptor_id,
            'persona_role_id'       => $request->role_id,
            'description'           => $request->description
        ]);

        if ($update) {

            $persona = self::find($request->id);

            return [
                'id'                => $persona->id,
                'media_icon_id'     => $persona->media->id,
                'media_icon'        => '//bsf.local/medias/' . $persona->media->image_file_name,
                'persona_number'    => (int)$persona->persona_number,
                'first_name'        => ucwords($persona->descriptor->name),
                'last_name'         => ucwords($persona->role->name),
                'description'       => $persona->description,
                'demographics'      => [
                    'gender'        => (int)$persona->demographics->gender,
                    'age'           => (int)$persona->demographics->age,  
                    'income'        => (int)$persona->demographics->income,
                    'education'     => (int)$persona->demographics->education
                ],
                'psychology'        => [
                    'needs'         => (int)$persona->psychology->needs,
                    'values'        => (int)$persona->psychology->values,
                    'worldview'     => (int)$persona->psychology->worldview,
                    'technicality'  => (int)$persona->psychology->technicality
                ],
                'pain'              => (string)$persona->details->pain,
                'desire'            => (string)$persona->details->desire,
                'assumptions'       => (string)$persona->details->assumptions,
                'keywords'          => (string)$persona->details->keywords,
                'status'            => true
            ];
        }

        return ['status' => false];
    }

    public static function updatePersonaNumber($request)
    {
        $update = self::where([
            'id' => $request->id
        ])
        ->update([
            'persona_number' => $request->persona_number
        ]);

        return ['status' => (boolean)$update];
    }

    public static function updateDemographic($request)
    {
        $update = Demographic::where('persona_id', $request->persona_id)
        ->update([
            'gender'    => $request->gender,
            'age'       => $request->age,
            'income'    => $request->income,
            'education' => $request->education
        ]);

        return ['status' => $update];
    }

    public static function updatePsychology($request)
    {
        $update = Psychology::where('persona_id', $request->persona_id)
        ->update([
            'needs'         => $request->needs,
            'values'        => $request->values,
            'worldview'     => $request->worldview,
            'technicality'  => $request->technicality
        ]);

        return ['status' => $update];
    }

    public static function updatePain($request)
    {
        $update = PersonaDetail::where('persona_id', $request->persona_id)
        ->update([
            'pain' => $request->pain
        ]);

        return ['status' => $update];
    }

    public static function updateDesire($request)
    {
        $update = PersonaDetail::where('persona_id', $request->persona_id)
        ->update([
            'desire' => $request->desire
        ]);

        return ['status' => $update];
    }

    public static function updateAssumption($request)
    {
        $update = PersonaDetail::where('persona_id', $request->persona_id)
        ->update([
            'assumptions' => $request->assumptions
        ]);

        return ['status' => $update];
    }

    public static function updateKeyword($request)
    {
        $update = PersonaDetail::where('persona_id', $request->persona_id)
        ->update([
            'keywords' => $request->keywords
        ]);

        return ['status' => $update];
    }

    public static function remove($id)
    {
        self::where('id', $id)->delete();
        PersonaDetail::where('persona_id', $id)->delete();
        Demographic::where('persona_id', $id)->delete();
        Psychology::where('persona_id', $id)->delete();

        return true;
    }
}
