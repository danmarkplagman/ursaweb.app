<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferFeature extends Model
{
    protected $fillable = [
        'feature',
        'benefits',
        'offer_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function offers()
    {
        return $this->hasMany('App\Offer');
    }
}
