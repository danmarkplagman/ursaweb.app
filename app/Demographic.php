<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demographic extends Model
{
    protected $fillable = [
        'gender',
        'age',
        'income',
        'education',
        'persona_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
