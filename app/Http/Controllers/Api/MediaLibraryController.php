<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\MediaLibraryServiceInterface;

class MediaLibraryController extends Controller
{
    private $_mediaLibraryService;

    /*
    * Constructor of services to be use on this controller
    *
    */
    public function __construct(MediaLibraryServiceInterface $mediaLibraryService)
    {
        /*
        * Construct MediaLibraryService class and use its method(s)
        */
        $this->_mediaLibraryService = $mediaLibraryService;
    }

    /*
    * Get Library Images
    *
    */
    public function getImages(Request $request)
    {
        return response()->json($this->_mediaLibraryService->getImages($request));
    }
}
