<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\MessageServiceInterface;

class MessageController extends Controller
{
    private $_messageService;

    /*
    * Constructor of services to be use on this controller
    *
    */
    public function __construct(MessageServiceInterface $messageService)
    {
        /*
        * Construct MessageService class and use its method(s)
        */
        $this->_messageService = $messageService;
    }

    /*
    * GET
    * Get Messages List
    * Params: company_id, type_id
    */
    public function lists(Request $request, $company_id, $type_id)
    {
        return response()->json($this->_messageService->lists($request, $company_id, $type_id));
    }

    /*
    * GET
    * Get First Message
    * Params: company_id, type_id
    */
    public function first(Request $request, $company_id, $type_id)
    {
        return response()->json($this->_messageService->first($request, $company_id, $type_id));
    }

    /*
    * GET
    * Get Message Types
    * 
    */
    public function types(Request $request)
    {
        return response()->json($this->_messageService->types($request));
    }

    /*
    * POST
    * Get Message Details
    * Params: id
    */
    public function details(Request $request)
    {
        return response()->json($this->_messageService->details($request));
    }

    /*
    * PATCH
    * Update Message
    * Params: body
    */
    public function update(Request $request)
    {
        return response()->json($this->_messageService->update($request));
    }

    /*
    * DELETE
    * Delete Message
    * Params: id
    */
    public function delete(Request $request, $id)
    {
        return response()->json($this->_messageService->delete($request, $id));
    }
}
