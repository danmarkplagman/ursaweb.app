<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\OfferServiceInterface;

class OfferController extends Controller
{
    private $_offerService;

    /*
    * Constructor of services to be use on this controller
    *
    */
    public function __construct(OfferServiceInterface $offerService)
    {
        /*
        * Construct OfferService class and use its method(s)
        */
        $this->_offerService = $offerService;
    }

    /*
    * GET
    * Get Offers List
    * Params: company_id, feature_id
    */
    public function lists(Request $request, $company_id, $category_id)
    {
        return response()->json($this->_offerService->lists($request, $company_id, $category_id));
    }

    /*
    * GET
    * Get Offers Types
    * Params:
    */
    public function types(Request $request)
    {
        return response()->json($this->_offerService->types($request));
    }

    /*
    * GET
    * Get Offers Categories
    * Params:
    */
    public function categories(Request $request)
    {
        return response()->json($this->_offerService->categories($request));
    }

    /*
    * GET
    * Get Offer Features
    * Params: 
    */
    public function features(Request $request)
    {
        return response()->json($this->_offerService->features($request));
    }

    /*
    * GET
    * Get Offer Details
    * Params: id
    */
    public function details(Request $request, $id)
    {
        return response()->json($this->_offerService->details($request, $id));
    }

    /*
    * POST
    * Create Offer
    * Params: body
    */
    public function create(Request $request)
    {
        return response()->json($this->_offerService->create($request));
    }

    /*
    * PATCH
    * Update Offer
    * Params: body
    */
    public function update(Request $request)
    {
        return response()->json($this->_offerService->update($request));
    }

    /*
    * DELETE
    * Delete Offer
    * Params: id
    */
    public function delete(Request $request, $id)
    {
        return response()->json($this->_offerService->delete($request, $id));
    }
}
