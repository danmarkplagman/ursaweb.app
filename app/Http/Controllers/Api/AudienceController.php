<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AudienceServiceInterface;

class AudienceController extends Controller
{
    private $_audienceService;

    /*
    * Constructor of services to be use on this controller
    *
    */
    public function __construct(AudienceServiceInterface $audienceService)
    {
        /*
        * Construct AudienceService class and use its method(s)
        */
        $this->_audienceService = $audienceService;
    }

    /*
    * GET
    * Get Persona Lists
    * Params: company_id
    */
    public function lists(Request $request, $company_id)
    {
        return response()->json($this->_audienceService->lists($request, $company_id));
    }

    /*
    * GET
    * Get Persona Names
    * Params: 
    */
    public function personaNames(Request $request)
    {
        return response()->json($this->_audienceService->personaNames($request));        
    }

    /*
    * POST
    * Get Persona Details
    * Params: id
    */
    public function details(Request $request)
    {
        return response()->json($this->_audienceService->details($request));
    }

    /*
    * POST
    * Create Persona
    * Params: body
    */
    public function create(Request $request)
    {
        return response()->json($this->_audienceService->create($request));
    }

    /*
    * PATCH
    * Update Persona Details
    * Params: body
    */
    public function updatePersona(Request $request)
    {
        return response()->json($this->_audienceService->updatePersona($request));
    }

    /*
    * PATCH
    * Update Persona Number
    * Params: body
    */
    public function updatePersonaNumber(Request $request)
    {
        return response()->json($this->_audienceService->updatePersonaNumber($request));
    }

    /*
    * PATCH
    * Update Persona Demographics
    * Params: body
    */
    public function updateDemographic(Request $request)
    {
        return response()->json($this->_audienceService->updateDemographic($request));
    }

    /*
    * PATCH
    * Update Persona Psychology
    * Params: body
    */
    public function updatePsychology(Request $request)
    {
        return response()->json($this->_audienceService->updatePsychology($request));
    }

    /*
    * PATCH
    * Update Persona Pain
    * Params: body
    */
    public function updatePain(Request $request)
    {
        return response()->json($this->_audienceService->updatePain($request));
    }

    /*
    * PATCH
    * Update Persona Desire
    * Params: body
    */
    public function updateDesire(Request $request)
    {
        return response()->json($this->_audienceService->updateDesire($request));
    }

    /*
    * PATCH
    * Update Persona Assumptions
    * Params: body
    */
    public function updateAssumption(Request $request)
    {
        return response()->json($this->_audienceService->updateAssumption($request));
    }

    /*
    * PATCH
    * Update Persona Keywords
    * Params: body
    */
    public function updateKeyword(Request $request)
    {
        return response()->json($this->_audienceService->updateKeyword($request));
    }

    /*
    * DELETE
    * Delete Persona
    * Params: id
    */
    public function delete(Request $request, $id)
    {
        return response()->json($this->_audienceService->delete($request, $id));
    }
}
