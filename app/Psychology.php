<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Psychology extends Model
{
    protected $table = 'psychology';

    protected $fillable = [
        'needs',
        'values',
        'worldview',
        'technicality',
        'persona_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
