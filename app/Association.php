<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{
    protected $fillable = [
        'name',
        'description',
        'website',
        'da',
        'company_id',
        'association_type_id',
        'association_category_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
