<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seed extends Model
{
    protected $fillable = [
        'title',
        'description',
        'status',
        'seed_format_id',
        'seed_channel_id',
        'media_library_id',
        'company_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
