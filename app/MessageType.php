<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageType extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /*
    * Model Relationships
    *
    */

    public function message()
    {
        return $this->hasOne('App\Message', 'message_type_id', 'id');
    }

    /*
    * Custom Methods
    *
    */
}
