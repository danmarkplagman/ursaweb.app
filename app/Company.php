<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'details',
        'logo',
        'email',
        'address',
        'contact_number',
        'user_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
