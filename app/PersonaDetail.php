<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaDetail extends Model
{
    protected $fillable = [
        'pain',
        'desire',
        'assumptions',
        'keywords',
        'persona_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
