<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image_path',
        'image_size',
        'image_status',
        'file_type',
        'image_type_id',
        'company_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
