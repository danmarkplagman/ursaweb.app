<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRelationFromOffersAndOfferFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->renameColumn('offer_feature_id', 'offer_category_id');
        });

        Schema::table('offer_features', function (Blueprint $table) {
            $table->integer('offer_id')->after('benefits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->renameColumn('offer_category_id', 'offer_feature_id');
        });

        Schema::table('offer_features', function (Blueprint $table) {
            $table->dropColumn(['offer_id']);
        });
    }
}
