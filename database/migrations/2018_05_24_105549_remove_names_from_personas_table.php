<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveNamesFromPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->dropColumn(['first_name', 'last_name']);
        });

        Schema::table('personas', function (Blueprint $table) {
            $table->integer('persona_name_id')->after('media_library_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->string('first_name');
            $table->string('last_name');
        });

        Schema::table('personas', function (Blueprint $table) {
            $table->dropColumn(['persona_name_id']);
        });
    }
}
