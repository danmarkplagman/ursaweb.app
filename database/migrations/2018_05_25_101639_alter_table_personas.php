<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->renameColumn('persona_name_id', 'persona_descriptor_id')->nullable();
        });

        Schema::table('personas', function (Blueprint $table) {
            $table->integer('persona_role_id')->after('persona_descriptor_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->dropColumn(['persona_role_id']);
            $table->renameColumn('persona_descriptor_id', 'persona_name_id');
        });
    }
}
