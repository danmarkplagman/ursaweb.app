<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePsychology extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psychology', function (Blueprint $table) {
            $table->increments('id');
            $table->text('needs')->nullable();
            $table->text('values')->nullable();
            $table->text('worldview')->nullable();
            $table->text('technicality')->nullable();
            $table->integer('persona_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psychologies');
    }
}
