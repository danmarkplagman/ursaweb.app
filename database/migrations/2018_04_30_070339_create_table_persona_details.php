<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonaDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('pain');
            $table->text('desire')->nullable();
            $table->text('assumptions')->nullable();
            $table->text('keywords')->nullable();
            $table->integer('persona_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona_details');
    }
}
