<?php

/*
* Audience Route
*/
Route::group(['prefix' => 'audience'], function () {

    Route::get('/lists/{company_id}', 'Api\AudienceController@lists')->name('audience.lists');
    Route::get('/names', 'Api\AudienceController@personaNames')->name('audience.persona.names');
    Route::post('/details', 'Api\AudienceController@details')->name('audience.details');
    Route::post('/create', 'Api\AudienceController@create')->name('audience.create');
    Route::patch('/persona/update', 'Api\AudienceController@updatePersona')->name('audience.update.persona');
    Route::patch('/persona-number/update', 'Api\AudienceController@updatePersonaNumber')->name('audience.update.persona_number');
    Route::patch('/demographic/update', 'Api\AudienceController@updateDemographic')->name('audience.update.demographic');
    Route::patch('/psychology/update', 'Api\AudienceController@updatePsychology')->name('audience.update.psychology');
    Route::patch('/pain/update', 'Api\AudienceController@updatePain')->name('audience.update.pain');
    Route::patch('/desire/update', 'Api\AudienceController@updateDesire')->name('audience.update.desire');
    Route::patch('/assumption/update', 'Api\AudienceController@updateAssumption')->name('audience.update.assumption');
    Route::patch('/keyword/update', 'Api\AudienceController@updateKeyword')->name('audience.update.keyword');
    Route::delete('/delete/{id}', 'Api\AudienceController@delete')->name('audience.delete');
});

/*
* Media Library Route
*/
Route::group(['prefix' => 'media-library'], function () {

    Route::group(['prefix' => 'images'], function () {

        Route::get('/get', 'Api\MediaLibraryController@getImages')->name('media.images.get');
    });
});

/*
* Message Route
*/
Route::group(['prefix' => 'message'], function () {

    Route::get('/lists/{company_id}/{type_id}', 'Api\MessageController@lists')->name('message.lists');
    Route::get('/first/{company_id}/{type_id}', 'Api\MessageController@first')->name('message.first');
    Route::get('/types', 'Api\MessageController@types')->name('message.types');
    Route::post('/details', 'Api\MessageController@details')->name('message.details');
    Route::patch('/update', 'Api\MessageController@update')->name('message.update');
    Route::delete('/delete/{id}', 'Api\MessageController@delete')->name('message.delete');
});

/*
* Offer Route
*/
Route::group(['prefix' => 'offer'], function () {

    Route::get('/lists/{company_id}/{category_id}', 'Api\OfferController@lists')->name('offer.lists');
    Route::get('/types', 'Api\OfferController@types')->name('offer.types');
    Route::get('/categories', 'Api\OfferController@categories')->name('offer.categories');
    Route::get('/features', 'Api\OfferController@features')->name('offer.features');
    Route::get('/details/{id}', 'Api\OfferController@details')->name('offer.details');
    Route::post('/create', 'Api\OfferController@create')->name('offer.create');
    Route::patch('/update', 'Api\OfferController@update')->name('offer.update');
    Route::delete('/delete/{id}', 'Api\OfferController@delete')->name('offer.delete');
});
