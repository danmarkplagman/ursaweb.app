<?php

/*
* Route initialization to read angular app inside public directory
*/
Route::any('{all}', 'HomeController@index')->where('all', '.*')->name('home');